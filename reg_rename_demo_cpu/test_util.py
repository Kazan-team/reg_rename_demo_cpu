# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information

from nmigen.hdl.ast import signed, unsigned
from reg_rename_demo_cpu.util import (reversed_range, value_in_range,
                                      value_range_for_shape)
import unittest


class TestUtil(unittest.TestCase):
    def ranges(self):
        for start in range(-10, 10):
            for stop in range(-10, 10):
                for step in range(-10, 10):
                    if step == 0:
                        continue
                    yield range(start, stop, step)

    def test_reversed_range(self):
        for forward_r in self.ranges():
            reversed_r = reversed_range(forward_r)
            with self.subTest(forward_r=forward_r,
                              reversed_r=reversed_r):
                self.assertEqual(list(reversed(forward_r)),
                                 list(reversed_r))

    def test_value_in_range(self):
        for r in self.ranges():
            for v in range(-20, 20):
                with self.subTest(r=r, v=v):
                    self.assertEqual(v in r, value_in_range(v, r))

    def test_value_range_for_shape(self):
        self.assertEqual(value_range_for_shape(unsigned(0)), range(0, 1))
        self.assertEqual(value_range_for_shape(signed(0)), range(0, 1))
        self.assertEqual(value_range_for_shape(unsigned(1)), range(0, 2))
        self.assertEqual(value_range_for_shape(signed(1)), range(-1, 1))
        self.assertEqual(value_range_for_shape(unsigned(2)), range(0, 4))
        self.assertEqual(value_range_for_shape(signed(2)), range(-2, 2))
        self.assertEqual(value_range_for_shape(unsigned(3)), range(0, 8))
        self.assertEqual(value_range_for_shape(signed(3)), range(-4, 4))
        self.assertEqual(value_range_for_shape(unsigned(8)), range(0, 0x100))
        self.assertEqual(value_range_for_shape(signed(8)), range(-0x80, 0x80))
        self.assertEqual(value_range_for_shape(unsigned(16)),
                         range(0, 0x10000))
        self.assertEqual(value_range_for_shape(signed(16)),
                         range(-0x8000, 0x8000))
