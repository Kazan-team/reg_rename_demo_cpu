# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information

import dataclasses
from typing import Callable, List, NoReturn
import unittest
from nmigen.sim.core import Delay, Settle, Tick
from reg_rename_demo_cpu.config import Config
from reg_rename_demo_cpu.fetch import BranchPredictor
from reg_rename_demo_cpu.util import get_sim


@dataclasses.dataclass(frozen=True, unsafe_hash=True)
class BPEntry:
    """branch predictor entry"""
    src_pc: int
    tgt_pc: int
    valid: bool

    def __repr__(self):
        return (f"BPEntry(src_pc={hex(self.src_pc)}, "
                f"tgt_pc={hex(self.tgt_pc)}, valid={self.valid})")


INVALID_ENTRY = BPEntry(src_pc=0, tgt_pc=0, valid=False)


@dataclasses.dataclass
class BPState:
    """branch predictor state"""
    entries: List[BPEntry]
    out_start_pc: int
    out_fetch_pc: int
    out_pred_br_src_pc: int
    out_pred_br_tgt_pc: int
    out_pred_br: bool
    out_valid: bool
    current_pc: int
    replace_index_ctr: int

    def __repr__(self):
        entries = ",\n        ".join(repr(i) for i in self.entries)
        return (f"BPState(entries=[\n"
                f"        {entries}\n"
                f"    ], "
                f"out_start_pc={hex(self.out_start_pc)},\n"
                f"    out_fetch_pc={hex(self.out_fetch_pc)}, "
                f"out_pred_br_src_pc={hex(self.out_pred_br_src_pc)},\n"
                f"    out_pred_br_tgt_pc={hex(self.out_pred_br_tgt_pc)}, "
                f"out_pred_br={self.out_pred_br},\n"
                f"    out_valid={self.out_valid}, "
                f"current_pc={hex(self.current_pc)},\n"
                f"    replace_index_ctr={self.replace_index_ctr})")


@dataclasses.dataclass
class BPInputs:
    """branch predictor inputs"""
    out_ready: bool
    in_need_training: bool
    in_src_pc: int
    in_tgt_pc: int
    in_branched: bool
    in_valid: bool

    def __repr__(self):
        return (f"BPInputs(\n"
                f"    out_ready={self.out_ready},\n"
                f"    in_need_training={self.in_need_training},\n"
                f"    in_src_pc={hex(self.in_src_pc)},\n"
                f"    in_tgt_pc={hex(self.in_tgt_pc)},\n"
                f"    in_branched={self.in_branched},\n"
                f"    in_valid={self.in_valid})")


class TestBranchPredictor(unittest.TestCase):
    def test(self):
        config = Config(instruction_memory_instructions=[0] * 32,
                        instruction_memory_start_address=0x1000,
                        reset_address=0x1000, log2_fetch_byte_count=5,
                        branch_predictor_entry_count=3)
        dut = BranchPredictor(config)

        def chk(failed_fn: Callable[[str], NoReturn], expected: BPState):
            yield Settle()
            with self.subTest(expected=repr(expected)):
                self.assertEqual(len(expected.entries),
                                 len(dut._state._entries))
                entries = []
                for entry in dut._state._entries:
                    entries.append(BPEntry(src_pc=(yield entry.src_pc),
                                           tgt_pc=(yield entry.tgt_pc),
                                           valid=bool((yield entry.valid))))
                self.assertEqual((yield dut.input.rv.ready), True)
                actual = BPState(
                    entries,
                    out_start_pc=(yield dut.output.data.start_pc),
                    out_fetch_pc=(yield dut.output.data.fetch_pc),
                    out_pred_br_src_pc=(yield dut.output.data.pred_br_src_pc),
                    out_pred_br_tgt_pc=(yield dut.output.data.pred_br_tgt_pc),
                    out_pred_br=bool((yield dut.output.data.pred_br)),
                    out_valid=bool((yield dut.output.rv.valid)),
                    current_pc=(yield dut._state._current_pc),
                    replace_index_ctr=(yield dut._state._replace_index_ctr))
                if expected != actual:
                    failed_fn(f"actual: {actual}")

        def write_inputs(inputs: BPInputs):
            yield Delay(0.1e-6)
            yield dut.output.rv.ready.eq(inputs.out_ready)
            yield dut.input.data.need_training.eq(inputs.in_need_training)
            yield dut.input.data.src_pc.eq(inputs.in_src_pc)
            yield dut.input.data.tgt_pc.eq(inputs.in_tgt_pc)
            yield dut.input.data.branched.eq(inputs.in_branched)
            yield dut.input.rv.valid.eq(inputs.in_valid)

        state = BPState(
            entries=[INVALID_ENTRY] * config.branch_predictor_entry_count,
            out_start_pc=0x0,
            out_fetch_pc=0x0, out_pred_br_src_pc=0x0,
            out_pred_br_tgt_pc=0x0, out_pred_br=False,
            out_valid=False, current_pc=0x1000,
            replace_index_ctr=0)

        fetch_mask = -1 << config.log2_fetch_byte_count

        def fill_state(out_valid: bool = True):
            state.out_valid = out_valid
            state.out_start_pc = state.current_pc
            state.out_fetch_pc = state.current_pc & fetch_mask
            next_fetch = state.out_fetch_pc + config.fetch_byte_count
            for pc in range(state.current_pc, next_fetch):
                for entry in state.entries:
                    if not entry.valid:
                        continue
                    if entry.src_pc == pc:
                        state.out_pred_br = True
                        state.out_pred_br_src_pc = entry.src_pc
                        state.out_pred_br_tgt_pc = entry.tgt_pc
                        return
            state.out_pred_br = False
            state.out_pred_br_src_pc = 0
            state.out_pred_br_tgt_pc = 0

        def advance_normally(failed_fn: Callable[[str], NoReturn]):
            fill_state()
            if state.out_pred_br:
                state.current_pc = state.out_pred_br_tgt_pc
            else:
                state.current_pc += config.fetch_byte_count
                state.current_pc &= fetch_mask
            fill_state()
            yield from write_inputs(BPInputs(
                out_ready=True, in_need_training=False,
                in_src_pc=0, in_tgt_pc=0,
                in_branched=False, in_valid=False))
            yield Tick()
            yield from chk(lambda msg: failed_fn(msg), state)

        def trap_to(pc: int, failed_fn: Callable[[str], NoReturn]):
            state.current_pc = pc
            fill_state(out_valid=False)
            yield from write_inputs(BPInputs(
                out_ready=True, in_need_training=False,
                in_src_pc=0, in_tgt_pc=pc,
                in_branched=True, in_valid=True))
            yield Tick()
            yield from chk(lambda msg: failed_fn(msg), state)
            fill_state()
            yield from write_inputs(BPInputs(
                out_ready=True, in_need_training=False,
                in_src_pc=0, in_tgt_pc=0,
                in_branched=False, in_valid=False))
            yield Tick()
            yield from chk(lambda msg: failed_fn(msg), state)

        def insert_branch(src_pc: int, tgt_pc: int):
            new_entry = BPEntry(src_pc=src_pc, tgt_pc=tgt_pc, valid=True)
            for index, entry in enumerate(state.entries):
                if entry.valid and entry.src_pc == src_pc:
                    state.entries[index] = new_entry
                    return
            for index, entry in enumerate(state.entries):
                if not entry.valid:
                    state.entries[index] = new_entry
                    return
            state.entries[state.replace_index_ctr] = new_entry
            state.replace_index_ctr += 1
            state.replace_index_ctr %= len(state.entries)

        def branch_to(src_pc: int, tgt_pc: int,
                      failed_fn: Callable[[str], NoReturn]):
            state.current_pc = tgt_pc
            insert_branch(src_pc, tgt_pc)
            fill_state(out_valid=False)
            yield from write_inputs(BPInputs(
                out_ready=True, in_need_training=True,
                in_src_pc=src_pc, in_tgt_pc=tgt_pc,
                in_branched=True, in_valid=True))
            yield Tick()
            yield from chk(lambda msg: failed_fn(msg), state)
            fill_state()
            yield from write_inputs(BPInputs(
                out_ready=True, in_need_training=False,
                in_src_pc=0, in_tgt_pc=0,
                in_branched=False, in_valid=False))
            yield Tick()
            yield from chk(lambda msg: failed_fn(msg), state)

        def unbranch_to(src_pc: int, tgt_pc: int,
                        failed_fn: Callable[[str], NoReturn]):
            state.current_pc = tgt_pc
            for index, entry in enumerate(state.entries):
                if entry.valid and entry.src_pc == src_pc:
                    state.entries[index] = BPEntry(src_pc=src_pc,
                                                   tgt_pc=tgt_pc,
                                                   valid=False)
                    break
            fill_state(out_valid=False)
            yield from write_inputs(BPInputs(
                out_ready=True, in_need_training=True,
                in_src_pc=src_pc, in_tgt_pc=tgt_pc,
                in_branched=False, in_valid=True))
            yield Tick()
            yield from chk(lambda msg: failed_fn(msg), state)
            fill_state()
            yield from write_inputs(BPInputs(
                out_ready=True, in_need_training=False,
                in_src_pc=0, in_tgt_pc=0,
                in_branched=False, in_valid=False))
            yield Tick()
            yield from chk(lambda msg: failed_fn(msg), state)

        def process():
            yield from write_inputs(BPInputs(
                out_ready=False, in_need_training=False,
                in_src_pc=0, in_tgt_pc=0,
                in_branched=False, in_valid=False))
            # check that we reset to valid with address 0x1000
            yield from chk(lambda msg: self.fail(msg), state)
            fill_state()
            yield Tick()
            # when out_ready=False, keep same state
            yield from chk(lambda msg: self.fail(msg), state)
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from trap_to(0x1248, lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from branch_to(0x1248, 0x1348, lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from trap_to(0x1248, lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from unbranch_to(0x1248, 0x1250, lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from branch_to(0x1248, 0x1230, lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from branch_to(0x1248, 0x1220, lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from branch_to(0x1220, 0x1348, lambda msg: self.fail(msg))
            yield from branch_to(0x1348, 0x1000, lambda msg: self.fail(msg))
            yield from branch_to(0x1000, 0x1348, lambda msg: self.fail(msg))
            yield from branch_to(0x1350, 0x1360, lambda msg: self.fail(msg))
            yield from branch_to(0x1360, 0x1370, lambda msg: self.fail(msg))
            yield from branch_to(0x1370, 0x1380, lambda msg: self.fail(msg))
            yield from branch_to(0x1380, 0x1390, lambda msg: self.fail(msg))
            yield from branch_to(0x1390, 0x13A0, lambda msg: self.fail(msg))
            yield from branch_to(0x13A0, 0x13B0, lambda msg: self.fail(msg))
            yield from unbranch_to(0x13A0, 0x13A8, lambda msg: self.fail(msg))
            yield from unbranch_to(0x1390, 0x1398, lambda msg: self.fail(msg))
            yield from unbranch_to(0x1380, 0x1388, lambda msg: self.fail(msg))
            yield from unbranch_to(0x1370, 0x1378, lambda msg: self.fail(msg))
            yield from unbranch_to(0x1360, 0x1368, lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from trap_to(0x1000, lambda msg: self.fail(msg))
            yield from branch_to(0x1010, 0x1010, lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from trap_to(0x1000, lambda msg: self.fail(msg))
            yield from branch_to(0x1010, 0x1010, lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from unbranch_to(0x1010, 0x1018, lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from branch_to(0x1050, 0x1000, lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from branch_to(0x1050, 0x1000, lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from branch_to(0x1000, 0x1010, lambda msg: self.fail(msg))
            yield from branch_to(0x1010, 0x1020, lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield from advance_normally(lambda msg: self.fail(msg))
            yield Tick()
        with get_sim(dut, self, traces=[*dut.signals()]) as sim:
            sim.add_process(process)
            sim.add_clock(1e-6)
            sim.run()


if __name__ == "__main__":
    unittest.main()
