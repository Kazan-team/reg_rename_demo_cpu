# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information

from enum import Enum
import itertools
from typing import Tuple, Union
import unittest

from nmigen.sim import Tick
from nmigen.sim.core import Delay, Settle
from reg_rename_demo_cpu.config import Config
from reg_rename_demo_cpu.decoder import Decoder
from reg_rename_demo_cpu.instruction import AtomicImmediate, Instruction, Opcode, OperandType, Register
from reg_rename_demo_cpu.util import get_sim
from reg_rename_demo_cpu.instruction_memory import InstructionMemory


class TestInstructionMemory(unittest.TestCase):
    def test_reading(self):
        config = Config()
        instructions = config.instruction_memory_instructions
        for i in range(15):
            instr = Instruction.encode(Opcode.Add, OperandType.U64,
                                       rt=Register.R3, immediate=i)
            instructions.append(instr)
        dut = InstructionMemory(config)

        def process():
            for address in range(-10, 160 + 1):
                address += config.instruction_memory_start_address
                yield dut.input.address.eq(address)
                yield dut.input.rv.valid.eq(True)
                yield dut.output.rv.ready.eq(True)
                yield Tick()
                yield Settle()
                outputs = []
                for o in dut.output.data.instrs:
                    outputs.append((yield o))
                error = yield dut.output.data.error
                adj_addr = address - config.instruction_memory_start_address
                valid_stop = config.instruction_memory_instructions_byte_size
                valid_range = range(0, valid_stop, config.fetch_byte_count)
                expected_error = adj_addr not in valid_range
                expected_outputs = [0] * config.fetch_instruction_count
                if not expected_error:
                    start = adj_addr // Instruction.BYTE_SIZE
                    for i in range(config.fetch_instruction_count):
                        if i + start < len(instructions):
                            expected_outputs[i] = instructions[i + start]
                with self.subTest(address=address, now=sim._engine.now):
                    self.assertEqual(error, expected_error)
                    if not expected_error:
                        self.assertEqual(outputs, expected_outputs)
        with get_sim(dut, self, traces=[
            *dut.input.signals(), *dut.output.signals(),
        ]) as sim:
            sim.add_process(process)
            sim.add_clock(1e-6)
            sim.run()

    def test_control(self):
        config = Config(log2_fetch_byte_count=5,
                        instruction_memory_start_address=0)
        instructions = config.instruction_memory_instructions
        for i in range(15):
            instr = Instruction.encode(Opcode.Add, OperandType.U64,
                                       rt=Register.R3, immediate=i)
            instructions.append(instr)
        dut = InstructionMemory(config)
        stop = config.instruction_memory_instructions_byte_size
        inputs = list(range(0, stop, config.fetch_byte_count))
        expected_outputs = []
        for address in inputs:
            o = [0] * config.fetch_instruction_count
            start = address // Instruction.BYTE_SIZE
            for i in range(config.fetch_instruction_count):
                if i + start < len(instructions):
                    o[i] = instructions[i + start]
            expected_outputs.append(o)

        def check_subcase(i_vld_values: Tuple[int, ...],
                          o_rdy_values: Tuple[int, ...]):
            # first, clear dut's state
            yield dut.input.rv.valid.eq(False)
            yield dut.output.rv.ready.eq(True)
            i_vld_bits = 0
            for v in i_vld_values:
                i_vld_bits <<= 1
                i_vld_bits |= v
            o_rdy_bits = 0
            for v in o_rdy_values:
                o_rdy_bits <<= 1
                o_rdy_bits |= v

            target_ticks = i_vld_bits * 0x1000 + o_rdy_bits * 0x100 + 0x10
            # print(f"tick_count={hex(tick_count)}",
            #       f"target_ticks={hex(target_ticks)}")
            for _ in range(tick_count, target_ticks):
                yield Tick()
            valid = yield dut.output.rv.valid
            self.assertFalse(valid)

            input_index = 0
            outputs = []

            def step(input_valid: int, output_ready: int):
                nonlocal input_index
                yield dut.output.rv.ready.eq(output_ready)
                if input_index < len(inputs):
                    yield dut.input.rv.valid.eq(input_valid)
                    yield dut.input.address.eq(inputs[input_index])
                else:
                    yield dut.input.rv.valid.eq(False)
                    yield dut.input.address.eq(0x789AFACE0123)
                yield Delay(0.1e-6)
                input_t = yield dut.input.rv.transfer_occurs
                output_t = yield dut.output.rv.transfer_occurs
                current_outputs = []
                for o in dut.output.data.instrs:
                    current_outputs.append((yield o))
                if input_t:
                    input_index += 1
                if output_t:
                    outputs.append(current_outputs)
                yield Tick()

            start_time = "unknown"
            try:
                start_time = sim._engine.now
            except AttributeError:
                pass
            # print(start_time)

            with self.subTest(start_time=start_time):
                for input_valid, output_ready in zip(i_vld_values,
                                                     o_rdy_values):
                    yield from step(input_valid, output_ready)

                for _ in range(10 + len(inputs)):
                    yield from step(1, 1)

                self.assertEqual(input_index, len(inputs))
                self.assertEqual(outputs, expected_outputs)

        def top_process():
            for i_vld_values in itertools.product(range(2),
                                                  repeat=len(inputs)):
                for o_rdy_values in itertools.product(range(2),
                                                      repeat=len(inputs)):
                    with self.subTest(i_vld_values=i_vld_values,
                                      o_rdy_values=o_rdy_values):
                        yield from check_subcase(i_vld_values=i_vld_values,
                                                 o_rdy_values=o_rdy_values)
        tick_count = 0

        def process():
            nonlocal tick_count
            g = top_process()
            send_value = None
            while True:
                try:
                    yielded = g.send(send_value)
                    if isinstance(yielded, Tick):
                        tick_count += 1
                    send_value = yield yielded
                except StopIteration:
                    return
                except Exception as e:
                    g.throw(e)

        with get_sim(dut, self, traces=[
            *dut.input.signals(), *dut.output.signals(),
        ]) as sim:
            sim.add_process(process)
            sim.add_clock(1e-6)
            sim.run()
