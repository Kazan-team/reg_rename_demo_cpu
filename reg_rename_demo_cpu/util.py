# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information

from contextlib import contextmanager
from typing import NoReturn, Optional, Union
from unittest import TestCase
from nmigen.hdl.ast import Shape, Signal, Value
from nmigen.hdl.ir import Elaboratable, Fragment
from nmigen.sim import Simulator
from nmigen.back.rtlil import convert
from nmutil.get_test_path import get_test_path


def assert_never(v: NoReturn) -> NoReturn:
    """helper for asserting that all enumerants are handled before reaching
    here. Makes Python type checkers check that all enumerants were handled.

    See: https://github.com/python/mypy/issues/6366#issuecomment-560369716
    """
    assert False, f"Unhandled type: {type(v).__name__}"


def value_range_for_shape(shape):
    shape = Shape.cast(shape)
    if shape.width == 0:
        return range(0, 1)
    if shape.signed:
        return range(-1 << (shape.width - 1), 1 << (shape.width - 1))
    return range(1 << shape.width)


def reversed_range(r: range):
    l = len(r)
    start = r.start + r.step * (l - 1)
    stop = start - r.step * l
    return range(start, stop, -r.step)


def value_in_range(value, r: range):
    if not isinstance(value, int):
        value = Value.cast(value)
    if r.step < 0:
        r = reversed_range(r)
    retval = value >= r.start
    retval &= value < r.stop
    if r.step != 1:
        if r.step == 1 << r.step.bit_length():
            mask = r.step - 1
            retval &= (value & mask) == (r.start & mask)
        else:
            retval &= (value - r.start) % r.step == 0
    return retval


@contextmanager
def get_sim(dut: Union[Elaboratable, Fragment],
            test_case: TestCase, *,
            traces=()):
    path = get_test_path(test_case, "sim_test_out")
    path.parent.mkdir(parents=True, exist_ok=True)
    il = convert(dut, ports=traces)
    path.with_suffix(".il").write_text(il, encoding="utf-8")
    sim = Simulator(dut)
    with sim.write_vcd(
            path.with_suffix(".vcd").open("wt", encoding="utf-8"),
            path.with_suffix(".gtkw").open("wt", encoding="utf-8"),
            traces=traces):
        yield sim


class ReadyValid:
    """ ready/valid signaling for managing data transfer between two pipeline
        stages.
    """

    def __init__(self, *, name: str = None, src_loc_at: int = 0,
                 ready_reset: bool = False, valid_reset: bool = False):
        name = deduce_name(name, 1 + src_loc_at)
        self.ready = Signal(name=f"{name}_ready", reset=ready_reset)
        """ output by the target pipe stage. When set, indicates that the
            target pipe stage wants to receive data on the next clock tick.
            Can depend combinatorially on `self.valid`. The data transfer
            happens when both `self.ready` and `self.valid` are set.
        """
        self.valid = Signal(name=f"{name}_valid", reset=valid_reset)
        """ output by the source pipe stage. When set, indicates that the
            source pipe stage wants to send data on the next clock tick. Must
            not depend combinatorially on `self.ready`.
        """

    @property
    def transfer_occurs(self) -> Value:
        """ set if a transfer will occur on the next clock tick. Is just
            `self.ready & self.valid`.
        """
        return self.ready & self.valid

    @property
    def src_blocked(self) -> Value:
        """ set if there won't be space for the source pipe stage to replace
            data/valid at the next clock tick.
            Is just `self.valid & ~self.ready`.

            Example:
            with m.If(~self.output.rv.src_blocked):
                m.d.sync += [
                    self.output.data.eq(data),
                    self.output.rv.valid.eq(True),
                ]
        """
        return self.valid & ~self.ready

    def signals(self):
        yield self.ready
        yield self.valid

    def eq(self, src: "ReadyValid"):
        """ Connect the source stage's output (`src`) to the target
            stage's input (`self`).

            Data will transfer like: `data_for(self).eq(data_for(src))`
        """
        return [src.ready.eq(self.ready), self.valid.eq(src.valid)]


def deduce_name(name: Optional[str], src_loc_at: int) -> str:
    """use in a class's `__init__` to deduce the name of the variable the new
    class instance is being assigned to.

    Example usage:
    class MyClass:
        def __init__(self, *, name: str = None, src_loc_at: int = 0):
            name = deduce_name(name, 1 + src_loc_at)
            self.sig = Signal(name=f"{name}_sig")

    var = MyClass()
    assert var.sig.name == "var_sig"
    """
    retval = Signal(name=name, src_loc_at=1 + src_loc_at).name
    assert isinstance(retval, str)
    return retval


LOG2_BITS_PER_BYTE = 3
BITS_PER_BYTE = 1 << LOG2_BITS_PER_BYTE
