# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information

from functools import reduce
from typing import Callable
from nmigen.hdl.ast import Cat, Const, Mux, Signal, Value
from nmigen.hdl.dsl import Module
from nmigen.hdl.ir import Elaboratable
from reg_rename_demo_cpu.config import Config
from reg_rename_demo_cpu.decoder import Decoder
from reg_rename_demo_cpu.instruction import Instruction
from reg_rename_demo_cpu.instruction_memory import InstructionMemory
from reg_rename_demo_cpu.util import ReadyValid, deduce_name


class FetchedInstruction:
    def __init__(self, name: str):
        self.instruction = Instruction(name=name)
        self.pc = Signal(Instruction.ADDRESS_SHAPE, name=f"{name}_pc")
        """ the address of the current instruction.
        """
        self.error = Signal(name=f"{name}_error")
        """ set if fetching this instruction failed.
        """
        self.next_pc = Signal(Instruction.ADDRESS_SHAPE,
                              name=f"{name}_next_pc")
        """ the address of the instruction immediately after the current
            instruction in memory.
        """
        self.pred_tgt_pc = Signal(Instruction.ADDRESS_SHAPE,
                                  name=f"{name}_pred_tgt_pc")
        """ the address that the fetch pipe predicted will be executed next.
        """

    def signals(self):
        yield from self.instruction.signals()
        yield self.pc
        yield self.error
        yield self.next_pc
        yield self.pred_tgt_pc

    def eq(self, rhs: "FetchedInstruction"):
        return [*self.instruction.eq(rhs.instruction),
                self.pc.eq(rhs.pc),
                self.error.eq(rhs.error),
                self.next_pc.eq(rhs.next_pc),
                self.pred_tgt_pc.eq(rhs.pred_tgt_pc)]


class FlushInputData:
    def __init__(self, *, name: str = None, src_loc_at: int = 0):
        name = deduce_name(name, 1 + src_loc_at)
        self.need_training = Signal(name=f"{name}_need_training")
        self.src_pc = Signal(Instruction.ADDRESS_SHAPE,
                             name=f"{name}_src_pc")
        self.tgt_pc = Signal(Instruction.ADDRESS_SHAPE,
                             name=f"{name}_tgt_pc")
        self.branched = Signal(name=f"{name}_branched")

    def signals(self):
        yield self.need_training
        yield self.src_pc
        yield self.tgt_pc
        yield self.branched

    def eq(self, rhs: "FlushInputData"):
        return [self.need_training.eq(rhs.need_training),
                self.src_pc.eq(rhs.src_pc),
                self.tgt_pc.eq(rhs.tgt_pc),
                self.branched.eq(rhs.branched)]


class FlushInput:
    def __init__(self, *, name: str = None, src_loc_at: int = 0):
        name = deduce_name(name, 1 + src_loc_at)
        self.data = FlushInputData(name=f"{name}_data")
        self.rv = ReadyValid(name=f"{name}_rv")

    def signals(self):
        yield from self.data.signals()
        yield from self.rv.signals()

    def eq(self, rhs: "FlushInput"):
        return [*self.data.eq(rhs.data),
                *self.rv.eq(rhs.rv)]


class BranchPredictorOutputData:
    def __init__(self, *, name: str = None, src_loc_at: int = 0):
        name = deduce_name(name, 1 + src_loc_at)
        self.start_pc = Signal(Instruction.ADDRESS_SHAPE,
                               name=f"{name}_start_pc")
        self.fetch_pc = Signal(Instruction.ADDRESS_SHAPE,
                               name=f"{name}_fetch_pc")
        self.pred_br_src_pc = Signal(Instruction.ADDRESS_SHAPE,
                                     name=f"{name}_pred_br_src_pc")
        self.pred_br_tgt_pc = Signal(Instruction.ADDRESS_SHAPE,
                                     name=f"{name}_pred_br_tgt_pc")
        self.pred_br = Signal(name=f"{name}_pred_br")

    def signals(self):
        yield self.start_pc
        yield self.fetch_pc
        yield self.pred_br_src_pc
        yield self.pred_br_tgt_pc
        yield self.pred_br

    def eq(self, rhs: "BranchPredictorOutputData"):
        return [self.start_pc.eq(rhs.start_pc),
                self.fetch_pc.eq(rhs.fetch_pc),
                self.pred_br_src_pc.eq(rhs.pred_br_src_pc),
                self.pred_br_tgt_pc.eq(rhs.pred_br_tgt_pc),
                self.pred_br.eq(rhs.pred_br)]


class BranchPredictorOutput:
    def __init__(self, *, name: str = None, src_loc_at: int = 0):
        name = deduce_name(name, 1 + src_loc_at)
        self.data = BranchPredictorOutputData(name=f"{name}_data")
        self.rv = ReadyValid(name=f"{name}_rv")

    def signals(self):
        yield from self.data.signals()
        yield from self.rv.signals()


class _BranchPredictorEntry:
    def __init__(self, *, name: str = None, src_loc_at: int = 0):
        name = deduce_name(name, 1 + src_loc_at)
        self.src_pc = Signal(Instruction.ADDRESS_SHAPE, name=f"{name}_src_pc")
        self.tgt_pc = Signal(Instruction.ADDRESS_SHAPE, name=f"{name}_tgt_pc")
        self.valid = Signal(name=f"{name}_valid", reset=False)

    def eq(self, rhs: "_BranchPredictorEntry"):
        return [self.src_pc.eq(rhs.src_pc),
                self.tgt_pc.eq(rhs.tgt_pc),
                self.valid.eq(rhs.valid)]

    def fetch_key(self, current_pc: Value, config: Config) -> Value:
        """ build a key for ordering entries when selecting which entry has
            highest priority for predicting branches. The one with the largest
            key is used.
        """
        cur_high_bits = current_pc[config.log2_fetch_byte_count:]
        cur_low_bits = current_pc[:config.log2_fetch_byte_count]
        src_high_bits = self.src_pc[config.log2_fetch_byte_count:]
        src_low_bits = self.src_pc[:config.log2_fetch_byte_count]
        matches = src_high_bits == cur_high_bits
        matches &= self.valid
        matches &= src_low_bits >= cur_low_bits
        return Mux(matches, Cat(~src_low_bits, Const(1, 1)), 0)


class _BranchPredictorEntryFinder(Elaboratable):
    """ Finds the entry with the largest key.
    """

    def __init__(self, state: "_BranchPredictorState",
                 entry_key: Callable[[_BranchPredictorEntry], Value]):
        self.state = state
        self.entry_key = entry_key
        entry_count = len(state._entries)
        self.index = Signal(range(entry_count))
        """ The index of the first entry with the largest key.
        """
        self._key_values = [entry_key(entry) for entry in self.state._entries]

        def make_key(i: int):
            return Signal(self._key_values[i].shape(), name=f"key_{i}")
        self._keys = [make_key(i) for i in range(entry_count)]
        merged_keys = reduce(lambda a, b: Mux(False, a, b), self._keys)

        def make_sig(i: int):
            return Signal(merged_keys.shape(), name=f"max_key_after_{i}")
        self._max_key_after = [make_sig(i) for i in range(entry_count)]
        self.found_max_key = Signal(merged_keys.shape())
        """ The largest key of all entries' keys.
        """
        assert not self.found_max_key.signed, "signed key not supported"
        self.entry = _BranchPredictorEntry()
        """ The first entry with the largest key.
        """

    def elaborate(self, platform):
        m = Module()
        max_key_after = Const(0)
        for index, entry in enumerate(self.state._entries):
            m.d.comb += self._keys[index].eq(self._key_values[index])
            m.d.comb += self._max_key_after[index].eq(
                Mux(self._keys[index] >= max_key_after,
                    self._keys[index],
                    max_key_after))
            max_key_after = self._max_key_after[index]
        m.d.comb += self.found_max_key.eq(max_key_after)
        if_fn = m.If
        for index, entry in enumerate(self.state._entries):
            with if_fn(self._keys[index] == self.found_max_key):
                m.d.comb += [
                    self.index.eq(index),
                    self.entry.eq(entry),
                ]
            if_fn = m.Elif
        return m


class _BranchPredictorState:
    def __init__(self, config: Config, *,
                 name: str = None, src_loc_at: int = 0):
        name = deduce_name(name, 1 + src_loc_at)
        self.config = config

        def make_ent(i: int):
            return _BranchPredictorEntry(name=f"{name}_entry_{i}")
        entry_count = config.branch_predictor_entry_count
        self._entries = [make_ent(i) for i in range(entry_count)]
        self._current_pc = Signal(Instruction.ADDRESS_SHAPE,
                                  name=f"{name}_current_pc",
                                  reset=config.reset_address)
        self._replace_index_ctr = Signal(range(entry_count),
                                         name=f"{name}_replace_index_ctr",
                                         reset=0)

    def eq(self, rhs: "_BranchPredictorState"):
        retval = [self._current_pc.eq(rhs._current_pc),
                  self._replace_index_ctr.eq(rhs._replace_index_ctr)]
        for l, r in zip(self._entries, rhs._entries):
            retval.extend(l.eq(r))
        return retval


class _BranchPredictorMakeOutput(Elaboratable):
    def __init__(self, state: _BranchPredictorState):
        self.state = state
        self.output = BranchPredictorOutputData()
        self._fetch_mask = -1 << self.state.config.log2_fetch_byte_count
        self._find_fetch_pc = _BranchPredictorEntryFinder(
            self.state, lambda e: e.fetch_key(state._current_pc, state.config))

    def elaborate(self, platform):
        m = Module()
        m.submodules.find_fetch_pc = self._find_fetch_pc
        m.d.comb += [
            self.output.start_pc.eq(self.state._current_pc),
            self.output.fetch_pc.eq(self.state._current_pc & self._fetch_mask),
            self.output.pred_br_src_pc.eq(0),
            self.output.pred_br_tgt_pc.eq(0),
            self.output.pred_br.eq(False),
        ]
        with m.If(self._find_fetch_pc.found_max_key != 0):
            m.d.comb += [
                self.output.pred_br_src_pc.eq(
                    self._find_fetch_pc.entry.src_pc),
                self.output.pred_br_tgt_pc.eq(
                    self._find_fetch_pc.entry.tgt_pc),
                self.output.pred_br.eq(True),
            ]
        return m


class _BranchPredictorPredictStep(Elaboratable):
    def __init__(self, input_state: _BranchPredictorState, *,
                 name: str = None, src_loc_at: int = 0):
        name = deduce_name(name, 1 + src_loc_at)
        self.config = input_state.config
        self.input_state = input_state
        self.output_state = _BranchPredictorState(self.config,
                                                  name=f"{name}_output_state")
        self._make_output = _BranchPredictorMakeOutput(self.input_state)

    def elaborate(self, platform):
        m = Module()
        m.submodules.make_output = self._make_output
        m.d.comb += self.output_state.eq(self.input_state)
        output = self._make_output.output
        m.d.comb += self.output_state._current_pc.eq(
            output.fetch_pc + self.config.fetch_byte_count)
        with m.If(output.pred_br):
            m.d.comb += self.output_state._current_pc.eq(output.pred_br_tgt_pc)
        return m


class _BranchPredictorFlushStep(Elaboratable):
    def __init__(self, input_state: _BranchPredictorState,
                 flush_data: FlushInputData, *,
                 name: str = None, src_loc_at: int = 0):
        name = deduce_name(name, 1 + src_loc_at)
        self.config = input_state.config
        self.input_state = input_state
        """input state -- not owned by self"""
        self.flush_data = flush_data
        self.output = _BranchPredictorState(self.config,
                                            name=f"{name}_output")
        entry_count = self.config.branch_predictor_entry_count
        self._replace_index = Signal(range(entry_count))
        self._do_replacement = Signal()

        def matches(entry: _BranchPredictorEntry):
            matches = entry.src_pc == self.flush_data.src_pc
            return entry.valid & matches
        self._find_flush_src_pc = _BranchPredictorEntryFinder(
            self.input_state, matches)
        self._find_invalid = _BranchPredictorEntryFinder(
            self.input_state, lambda e: ~e.valid)

    def elaborate(self, platform):
        m = Module()
        m.submodules.find_flush_src_pc = self._find_flush_src_pc
        m.submodules.find_invalid = self._find_invalid
        entry_count = self.config.branch_predictor_entry_count
        m.d.comb += [
            self.output.eq(self.input_state),
            self.output._current_pc.eq(self.flush_data.tgt_pc),
        ]
        with m.If(self.flush_data.need_training):
            m.d.comb += self._do_replacement.eq(self.flush_data.branched),
            with m.If(self._find_flush_src_pc.found_max_key):
                m.d.comb += [
                    self._replace_index.eq(self._find_flush_src_pc.index),
                    self._do_replacement.eq(True),
                ]
            with m.Elif(self._find_invalid.found_max_key):
                m.d.comb += [
                    self._replace_index.eq(self._find_invalid.index),
                ]
            with m.Else():
                in_replace_index_ctr = self.input_state._replace_index_ctr
                out_replace_index_ctr = self.output._replace_index_ctr
                m.d.comb += [
                    self._replace_index.eq(in_replace_index_ctr),
                ]
                next_replace_index_ctr = in_replace_index_ctr + 1
                with m.If(next_replace_index_ctr >= entry_count):
                    m.d.comb += out_replace_index_ctr.eq(0)
                with m.Else():
                    m.d.comb += out_replace_index_ctr.eq(
                        next_replace_index_ctr)

            with m.If(self._do_replacement):
                for index, entry in enumerate(self.output._entries):
                    with m.If(index == self._replace_index):
                        m.d.comb += [
                            entry.src_pc.eq(self.flush_data.src_pc),
                            entry.tgt_pc.eq(self.flush_data.tgt_pc),
                            entry.valid.eq(self.flush_data.branched),
                        ]
        return m


class BranchPredictor(Elaboratable):
    def __init__(self, config: Config):
        self.config = config
        self.input = FlushInput()
        self.output = BranchPredictorOutput()

        self._state = _BranchPredictorState(config)
        self._output_valid = Signal(reset=False)
        self._next_state = _BranchPredictorState(config)
        self._make_next_output = _BranchPredictorMakeOutput(self._next_state)
        self._predict_step = _BranchPredictorPredictStep(self._state)
        self._flush_step = _BranchPredictorFlushStep(self._state,
                                                     self.input.data)

    def signals(self):
        yield from self.input.signals()
        yield from self.output.signals()

    def elaborate(self, platform):
        m = Module()
        m.submodules.make_next_output = self._make_next_output
        m.submodules.predict_step = self._predict_step
        m.submodules.flush_step = self._flush_step
        m.d.comb += [
            self.input.rv.ready.eq(True),
            self.output.rv.valid.eq(self._output_valid),
            self._next_state.eq(self._state),
        ]
        m.d.sync += self._state.eq(self._next_state)
        m.d.sync += self.output.data.eq(self._make_next_output.output)
        m.d.sync += self._output_valid.eq(True)
        with m.If(self.input.rv.valid):
            m.d.comb += [
                self.output.rv.valid.eq(False),
                self._next_state.eq(self._flush_step.output),
            ]
            m.d.sync += self._output_valid.eq(False)
        with m.Elif(~self.output.rv.src_blocked & self._output_valid):
            m.d.comb += [
                self._next_state.eq(self._predict_step.output_state),
            ]
        return m


class FetchPipe(Elaboratable):
    def __init__(self, config: Config):
        self.config = config
        self.instruction_memory = InstructionMemory(config)
        self.input = FlushInput()
        self.branch_predictor = BranchPredictor(config)
        output_count = config.fetch_instruction_count
        raise NotImplementedError("TODO: finish implementing")

        def instr(name: str):
            return FetchedInstruction(name=name)
        self.outputs = [instr(f"output_{i}") for i in range(output_count)]
        self._move_to_outs = Signal()
        self._move_to_outs = Signal()
        self.output_rv = ReadyValid()
        self._output_valid = Signal(reset=False)
        self._next_output_valid = Signal(reset=False)
        self._decoders = [Decoder() for i in range(output_count)]
        self._imem_out_valid = Signal(reset=False)
        self._next_imem_out_valid = Signal(reset=False)

    def elaborate(self, platform):
        m = Module()
        raise NotImplementedError("TODO: finish implementing")
        m.submodules.instruction_memory = self.instruction_memory
        m.submodules.branch_predictor = self.branch_predictor
        for i, decoder in enumerate(self._decoders):
            setattr(m.submodules, f"decoder_{i}", decoder)
            m.d.comb += decoder.input.eq(
                self.instruction_memory.output.instrs[i])
            with m.If(self._move_to_outs):
                m.d.sync += self.outputs[i].instruction.eq(decoder.output)
        m.d.comb += [
            self.branch_predictor.input.eq(self.input),
            self.instruction_memory.input.address.eq(
                self.branch_predictor.output.data.fetch_pc),
        ]
        m.d.sync += [
            self._imem_out_valid.eq(self._next_imem_out_valid),
            self._output_valid.eq(self._next_output_valid),
        ]
        with m.If(self.input.rv.transfer_occurs):
            m.d.comb += [
                self.output_rv.valid.eq(False),
                self._next_imem_out_valid.eq(False),
                self._next_output_valid.eq(False),
                self.branch_predictor.output.rv.ready.eq(False),
            ]
        with m.Else():
            m.d.comb += self.output_rv.valid.eq(self._output_valid)
            with m.Switch(Cat(self.branch_predictor.output.rv.valid,
                              self._imem_out_valid,
                              self._output_valid,
                              self.output_rv.ready)):
                with m.Case('0000'):
                    m.d.comb += [  # everything empty
                        self.branch_predictor.output.rv.ready.eq(True),
                        self._next_imem_out_valid.eq(False),
                        self._next_output_valid.eq(False),
                    ]
                # ... TODO
                with m.Case('1111'):
                    m.d.comb += [  # TODO
                        self.branch_predictor.output.rv.ready.eq(True),
                        self._next_imem_out_valid.eq(False),
                        self._next_output_valid.eq(False),
                    ]
        return m
