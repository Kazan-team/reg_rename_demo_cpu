# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information

from typing import Iterable
from nmigen.hdl.ast import Cat, Signal
from nmigen.hdl.dsl import Module
from nmigen.hdl.ir import Elaboratable
from nmigen.hdl.mem import Memory
from reg_rename_demo_cpu.config import Config
from reg_rename_demo_cpu.instruction import Instruction
from reg_rename_demo_cpu.util import ReadyValid, deduce_name


class InstructionMemoryInput:
    def __init__(self, *, name: str = None, src_loc_at: int = 0):
        name = deduce_name(name, 1 + src_loc_at)
        self.address = Signal(Instruction.ADDRESS_SHAPE,
                              name=f"{name}_address")
        self.rv = ReadyValid(name=f"{name}_rv")

    def signals(self):
        yield self.address
        yield from self.rv.signals()


class InstructionMemoryOutputData:
    def __init__(self, output_count: int, *,
                 name: str = None, src_loc_at: int = 0):
        name = deduce_name(name, 1 + src_loc_at)

        def make_sig(i: int):
            return Signal(Instruction.ENCODED_SHAPE, name=f"{name}_instr_{i}")
        self.instrs = [make_sig(i) for i in range(output_count)]
        self.error = Signal(name=f"{name}_error")

    def eq(self, rhs: "InstructionMemoryOutputData"):
        retval = [self.error.eq(rhs.error)]
        for l, r in zip(self.instrs, rhs.instrs):
            retval.append(l.eq(r))
        return retval

    def signals(self):
        yield from self.instrs
        yield self.error


class InstructionMemoryOutput:
    def __init__(self, output_count: int, *,
                 name: str = None, src_loc_at: int = 0):
        name = deduce_name(name, 1 + src_loc_at)
        self.data = InstructionMemoryOutputData(output_count, name=name)
        self.rv = ReadyValid(name=f"{name}_rv")

    def signals(self):
        yield from self.data.signals()
        yield from self.rv.signals()


class InstructionMemory(Elaboratable):
    def __init__(self, config: Config):
        self.input = InstructionMemoryInput()
        self.config = config
        self.output = InstructionMemoryOutput(config.fetch_instruction_count)
        self._delayed_out = InstructionMemoryOutputData(
            config.fetch_instruction_count)
        self._delayed_out_valid = Signal(reset=False)
        self._current_out = InstructionMemoryOutputData(
            config.fetch_instruction_count)
        self._next_valid = Signal()
        self._next_delayed_out_valid = Signal()

    def elaborate(self, platform):
        m = Module()
        width = 8 * self.config.fetch_byte_count
        fetch_instr_count = self.config.fetch_instruction_count
        log2_fetch_byte_count = self.config.log2_fetch_byte_count
        depth = self.config.instruction_memory_depth
        start_address = self.config.instruction_memory_start_address
        instr_bytes = self.config.instruction_memory_instructions_byte_size
        init = [0] * depth
        for i, v in enumerate(self.config.instruction_memory_instructions):
            shift = Instruction.ENCODED_WIDTH * (i % fetch_instr_count)
            init[i // fetch_instr_count] |= v << shift
        mem = Memory(width=width, depth=depth, init=init)
        read_port = mem.read_port(transparent=False)
        m.submodules.read_port = read_port
        adj_addr = Signal(Instruction.ADDRESS_SHAPE.width)
        m.d.comb += adj_addr.eq(self.input.address - start_address)
        m.d.sync += self._current_out.error.eq(True)
        m.d.comb += read_port.addr.eq(adj_addr[log2_fetch_byte_count:])
        aligned = adj_addr[:log2_fetch_byte_count] == 0
        in_range = adj_addr < instr_bytes
        with m.If(aligned & in_range):
            m.d.sync += self._current_out.error.eq(False)
            m.d.comb += read_port.en.eq(True)
        with m.If(~self._current_out.error):
            m.d.comb += Cat(*self._current_out.instrs).eq(read_port.data)

        m.d.sync += self.output.rv.valid.eq(self._next_valid)
        m.d.sync += self._delayed_out.eq(self.output.data)
        m.d.sync += self._delayed_out_valid.eq(self._next_delayed_out_valid)
        with m.If(self._delayed_out_valid):
            m.d.comb += self.output.data.eq(self._delayed_out)
        with m.Else():
            m.d.comb += self.output.data.eq(self._current_out)
        X0 = False  # don't care that we set to False
        X1 = True  # don't care that we set to True
        _0 = False  # abbreviation for False
        _1 = True  # abbreviation for True

        CASES = {
            # i_vld, dly_vld, o_vld, o_rdy
            # outputs:        i_rdy, n_dly_vld, n_vld
            (_0, _0, _0, _0): (_1, _0, _0),  # empty
            (_0, _0, _0, _1): (_1, _0, _0),  # empty
            (_0, _0, _1, _0): (_0, _1, _1),  # out -> delay_out
            (_0, _0, _1, _1): (_1, _0, _0),  # out -> next stage
            (_0, _1, _0, _0): (_0, _1, _1),  # stay in delay_out
            (_0, _1, _0, _1): (_1, _0, _0),  # delay_out -> next stage
            (_0, _1, _1, _0): (_0, _1, _1),  # stay in delay_out
            (_0, _1, _1, _1): (_1, _0, _0),  # delay_out -> next stage
            (_1, _0, _0, _0): (_1, _0, _1),  # prev stage -> out
            (_1, _0, _0, _1): (_1, _0, _1),  # prev stage -> out
            (_1, _0, _1, _0): (_0, _1, _1),  # out -> delay_out
            (_1, _0, _1, _1): (_1, _0, _1),  # prev stage -> out -> next stage
            (_1, _1, _0, _0): (_0, _1, _1),  # stay in delay_out
            (_1, _1, _0, _1): (_1, _0, _1),  # prv stage->out; dly_o->nxt stage
            (_1, _1, _1, _0): (_0, _1, _1),  # stay in delay_out
            (_1, _1, _1, _1): (_1, _0, _1),  # prv stage->out; dly_o->nxt stage
        }
        assert len(CASES) == 2 ** 4
        for index, (k, v) in enumerate(CASES.items()):
            assert len(k) == 4
            assert len(v) == 3
            key_index = 0
            for i in k:
                key_index <<= 1
                key_index |= i
            assert index == key_index, f"wrong key {k}, index={bin(index)}"
            i_vld, dly_vld, o_vld, o_rdy = k
            i_rdy, n_dly_vld, n_vld = v
            matches = self.input.rv.valid == i_vld
            matches &= self._delayed_out_valid == dly_vld
            matches &= self.output.rv.valid == o_vld
            matches &= self.output.rv.ready == o_rdy
            with m.If(matches):
                m.d.comb += [self.input.rv.ready.eq(i_rdy),
                             self._next_delayed_out_valid.eq(n_dly_vld),
                             self._next_valid.eq(n_vld)]
        return m
