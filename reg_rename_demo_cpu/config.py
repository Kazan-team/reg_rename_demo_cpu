# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information

import dataclasses
from typing import List
from reg_rename_demo_cpu.instruction import Instruction
from reg_rename_demo_cpu.util import LOG2_BITS_PER_BYTE


@dataclasses.dataclass
class Config:
    instruction_memory_instructions: List[int] \
        = dataclasses.field(default_factory=list)
    log2_fetch_byte_count: int = 5
    instruction_memory_start_address: int = 0x1000_0000
    reset_address: int = 0x1000_0000
    branch_predictor_entry_count: int = 4

    @property
    def fetch_byte_count(self):
        return 1 << self.log2_fetch_byte_count

    @property
    def fetch_bit_width(self):
        return 1 << self.log2_fetch_bit_width

    @property
    def log2_fetch_bit_width(self):
        return self.log2_fetch_byte_count + LOG2_BITS_PER_BYTE

    @property
    def log2_fetch_instruction_count(self):
        return self.log2_fetch_byte_count - Instruction.LOG2_BYTE_SIZE

    @property
    def fetch_instruction_count(self):
        return 1 << self.log2_fetch_instruction_count

    @property
    def instruction_memory_instructions_byte_size(self):
        return len(self.instruction_memory_instructions) \
            * Instruction.BYTE_SIZE

    @property
    def instruction_memory_depth(self):
        """ number of `fetch_bit_width`-sized words needed to store all
            instructions.
        """
        count = len(self.instruction_memory_instructions)
        return -(-count // self.fetch_instruction_count)
