# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information
import enum
from typing import Iterable, Union
from nmigen.hdl.ast import Assign, Cat, Const, Shape, Signal, signed, unsigned
from reg_rename_demo_cpu.util import assert_never, deduce_name, value_range_for_shape


@enum.unique
class Opcode(enum.Enum):
    Illegal = 0x0
    Add = 0x1
    """ rt = ra + rb + immediate
    """
    Sub = 0x2
    """ rt = ra - rb - immediate
    """
    MulAdd = 0x3
    """ rt = ra * rb + rc + immediate
    """
    MulSub = 0x4
    """ rt = ra * rb - rc - immediate
    """
    Div = 0x5
    """ rt = trunc(ra / rb)
    """
    Rem = 0x6
    """ rt = ra - trunc(ra / rb) * rb
    """
    And = 0x7
    """ rt = ra & rb & immediate
    """
    Or = 0x8
    """ rt = ra | rb | immediate
    """
    Xor = 0x9
    """ rt = ra ^ rb ^ immediate
    """
    ShiftLeft = 0xA
    """ rt = ra << (rb + immediate)
    """
    ShiftRight = 0xB
    """ rt = ra >> (rb + immediate)
    """
    Load = 0xC
    """ rt = mem[ra + rb + immediate]
    """
    Store = 0xD
    """ mem[ra + rb + immediate] = rc
    """
    Fence = 0xE
    """ mem.fence(AtomicImmediate(immediate))
    """
    AtomicFetchAdd = 0xF
    """ rt = mem.fetch_add(addr=ra + rb, amount=rc,
                           ordering=AtomicImmediate(immediate))
    """
    LoadLinked = 0x10
    """ rt = mem.load_linked(addr=ra + rb,
                             ordering=AtomicImmediate(immediate))
    """
    StoreConditional = 0x11
    """ rt = mem.store_conditional(addr=ra + rb, value=rc,
                                   ordering=AtomicImmediate(immediate))
    """
    BranchLT = 0x12
    """ rt = next_pc
        if ra < rb:
            if rc is Register.Zero:
                next_pc = pc + immediate
            else:
                next_pc = rc + immediate
    """
    BranchLE = 0x13
    """ rt = next_pc
        if ra <= rb:
            if rc is Register.Zero:
                next_pc = pc + immediate
            else:
                next_pc = rc + immediate
    """
    BranchEq = 0x14
    """ rt = next_pc
        if ra == rb:
            if rc is Register.Zero:
                next_pc = pc + immediate
            else:
                next_pc = rc + immediate
    """
    BranchNE = 0x15
    """ rt = next_pc
        if ra != rb:
            if rc is Register.Zero:
                next_pc = pc + immediate
            else:
                next_pc = rc + immediate
    """
    ReadControl = 0x16
    """ rt = control[immediate]
    """
    ReplaceControl = 0x17
    """ rt = control[immediate]
        control[immediate] = ra
    """
    IllegalLast = 0xFF

    @property
    def input_registers_count(self):
        if (
            self == Opcode.Illegal
            or self == Opcode.Fence
            or self == Opcode.ReadControl
            or self == Opcode.IllegalLast
        ):
            return 0
        elif (
            self == Opcode.ReplaceControl
        ):
            return 1
        elif (
            self == Opcode.Add
            or self == Opcode.Sub
            or self == Opcode.Div
            or self == Opcode.Rem
            or self == Opcode.And
            or self == Opcode.Or
            or self == Opcode.Xor
            or self == Opcode.ShiftLeft
            or self == Opcode.ShiftRight
            or self == Opcode.Load
            or self == Opcode.LoadLinked
        ):
            return 2
        elif (
            self == Opcode.MulAdd
            or self == Opcode.MulSub
            or self == Opcode.Store
            or self == Opcode.AtomicFetchAdd
            or self == Opcode.StoreConditional
            or self == Opcode.BranchLT
            or self == Opcode.BranchLE
            or self == Opcode.BranchEq
            or self == Opcode.BranchNE
        ):
            return 3
        else:
            assert_never(self)

    @property
    def has_ra(self):
        return self.input_registers_count >= 1

    @property
    def has_rb(self):
        return self.input_registers_count >= 2

    @property
    def has_rc(self):
        return self.input_registers_count >= 3

    @property
    def has_rt(self):
        if (
            self == Opcode.Illegal
            or self == Opcode.Store
            or self == Opcode.Fence
            or self == Opcode.IllegalLast
        ):
            return False
        elif (
            self == Opcode.Add
            or self == Opcode.Sub
            or self == Opcode.MulAdd
            or self == Opcode.MulSub
            or self == Opcode.Div
            or self == Opcode.Rem
            or self == Opcode.And
            or self == Opcode.Or
            or self == Opcode.Xor
            or self == Opcode.ShiftLeft
            or self == Opcode.ShiftRight
            or self == Opcode.Load
            or self == Opcode.AtomicFetchAdd
            or self == Opcode.LoadLinked
            or self == Opcode.StoreConditional
            or self == Opcode.BranchLT
            or self == Opcode.BranchLE
            or self == Opcode.BranchEq
            or self == Opcode.BranchNE
            or self == Opcode.ReadControl
            or self == Opcode.ReplaceControl
        ):
            return True
        else:
            assert_never(self)

    @property
    def immediate_range(self):
        if (
            self == Opcode.Illegal
            or self == Opcode.Div
            or self == Opcode.Rem
            or self == Opcode.ReadControl
            or self == Opcode.ReplaceControl
            or self == Opcode.IllegalLast
        ):
            return value_range_for_shape(0)
        elif (
            self == Opcode.Add
            or self == Opcode.Sub
            or self == Opcode.MulAdd
            or self == Opcode.MulSub
            or self == Opcode.And
            or self == Opcode.Or
            or self == Opcode.Xor
            or self == Opcode.ShiftLeft
            or self == Opcode.ShiftRight
            or self == Opcode.Load
            or self == Opcode.Store
            or self == Opcode.BranchLT
            or self == Opcode.BranchLE
            or self == Opcode.BranchEq
            or self == Opcode.BranchNE
        ):
            return value_range_for_shape(Instruction.IMMEDIATE_SHAPE)
        elif (
            self == Opcode.Fence
            or self == Opcode.AtomicFetchAdd
            or self == Opcode.LoadLinked
            or self == Opcode.StoreConditional
        ):
            return value_range_for_shape(AtomicImmediate)
        else:
            assert_never(self)

    @property
    def is_illegal(self):
        return self == Opcode.Illegal or self == Opcode.IllegalLast

    @property
    def supported_operand_types(self):
        if (
            self == Opcode.Illegal
            or self == Opcode.IllegalLast
        ):
            return ()
        elif (
            self == Opcode.Add
            or self == Opcode.Sub
            or self == Opcode.MulAdd
            or self == Opcode.MulSub
            or self == Opcode.Div
            or self == Opcode.Rem
            or self == Opcode.And
            or self == Opcode.Or
            or self == Opcode.Xor
            or self == Opcode.ShiftLeft
            or self == Opcode.ShiftRight
            or self == Opcode.Load
            or self == Opcode.Store
            or self == Opcode.AtomicFetchAdd
            or self == Opcode.LoadLinked
            or self == Opcode.StoreConditional
            or self == Opcode.BranchLT
            or self == Opcode.BranchLE
            or self == Opcode.BranchEq
            or self == Opcode.BranchNE
        ):
            return INTEGER_OPERAND_TYPES
        elif (
            self == Opcode.Fence
        ):
            return (OperandType(0),)
        elif (
            self == Opcode.ReadControl
            or self == Opcode.ReplaceControl
        ):
            return (OperandType.U64,)
        else:
            assert_never(self)


assert Shape.cast(Opcode) == unsigned(8)
for i, v in enumerate(Opcode):
    if v == Opcode.IllegalLast:
        i = (2 ** Shape.cast(Opcode).width) - 1
    assert i == v.value, f"{v} should have value {i}"
del i, v


@enum.unique
class OperandType(enum.Enum):
    U8 = 0x0
    U16 = 0x1
    U32 = 0x2
    U64 = 0x3
    I8 = 0x4
    I16 = 0x5
    I32 = 0x6
    I64 = 0x7
    IllegalLast = 0xF


assert Shape.cast(OperandType) == unsigned(4)

INTEGER_OPERAND_TYPES = (
    OperandType.U8,
    OperandType.U16,
    OperandType.U32,
    OperandType.U64,
    OperandType.I8,
    OperandType.I16,
    OperandType.I32,
    OperandType.I64,
)


@enum.unique
class Register(enum.Enum):
    Zero = 0
    Link = 1
    SP = 2
    R3 = 3
    R4 = 4
    R5 = 5
    R6 = 6
    R7 = 7
    R8 = 8
    R9 = 9
    R10 = 10
    R11 = 11
    R12 = 12
    R13 = 13
    R14 = 14
    R15 = 15


assert Shape.cast(Register) == unsigned(4)
assert len(list(Register)) == 2 ** Shape.cast(Register).width, "missing regs"


class AtomicImmediate(enum.Flag):
    IFence = 0
    """Instruction fence -- only for Fence instruction"""
    Relaxed = 0
    """only for instructions other than Fence"""
    Acquire = 1
    Release = 2
    SeqCst = 3


class Instruction:
    IMMEDIATE_SHAPE = signed(32 + 4)
    LOG2_BYTE_SIZE = 3
    BYTE_SIZE = 2 ** LOG2_BYTE_SIZE
    LOG2_ENCODED_WIDTH = 3 + LOG2_BYTE_SIZE
    ENCODED_WIDTH = 2 ** LOG2_ENCODED_WIDTH
    ENCODED_SHAPE = unsigned(ENCODED_WIDTH)
    ADDRESS_SHAPE = signed(64)

    def __init__(self, *, name: str = None, src_loc_at: int = 0):
        name = deduce_name(name, 1 + src_loc_at)
        self.opcode = Signal(Opcode, name=f"{name}_opcode")
        self.operand_type = Signal(OperandType, name=f"{name}_operand_type")
        self.rt = Signal(Register, name=f"{name}_rt")
        self.ra = Signal(Register, name=f"{name}_ra")
        self.rb = Signal(Register, name=f"{name}_rb")
        self.rc = Signal(Register, name=f"{name}_rc")
        self.immediate = Signal(self.IMMEDIATE_SHAPE, name=f"{name}_immediate")

    def signals(self):
        yield self.opcode
        yield self.operand_type
        yield self.rt
        yield self.ra
        yield self.rb
        yield self.rc
        yield self.immediate

    @staticmethod
    def encode(opcode: Opcode,
               operand_type: OperandType = OperandType(0), *,
               rt: Register = Register.Zero,
               ra: Register = Register.Zero,
               rb: Register = Register.Zero,
               rc: Register = Register.Zero,
               immediate: Union[int, AtomicImmediate] = 0):
        retval = 0
        pushed_bits = 0

        def push_bits(shape, value):
            nonlocal retval, pushed_bits
            shape = Shape.cast(shape)
            if isinstance(value, enum.Enum):
                value = value.value
            assert isinstance(value, int)
            assert Const.normalize(value, shape) == value, "value out of range"
            value = Const.normalize(value, unsigned(shape.width))
            retval |= value << pushed_bits
            pushed_bits += shape.width

        push_bits(Opcode, opcode)
        push_bits(OperandType, operand_type)
        push_bits(Register, rt)
        push_bits(Register, ra)
        push_bits(Register, rb)
        push_bits(Register, rc)
        push_bits(Instruction.IMMEDIATE_SHAPE, immediate)
        return retval

    def encoded(self):
        """returns the Cat of all signals, this can be assigned to"""
        return Cat(*self.signals())

    def eq(self, rhs: "Instruction"):
        assert isinstance(rhs, Instruction)
        return [l.eq(r) for l, r in zip(self.signals(), rhs.signals())]


assert Instruction().encoded().shape() == Instruction.ENCODED_SHAPE
