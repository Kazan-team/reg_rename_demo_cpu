# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information

from enum import Enum
from typing import Union
import unittest
from reg_rename_demo_cpu.decoder import Decoder
from nmigen.sim import Delay
from reg_rename_demo_cpu.instruction import AtomicImmediate, Instruction, Opcode, OperandType, Register
from reg_rename_demo_cpu.util import get_sim


class TestDecoder(unittest.TestCase):
    def case(self,
             encoded: int,
             illegal: bool,
             **expected):
        def values_str(values):
            items = []
            for k, v in expected.items():
                if isinstance(v, int):
                    items.append(f"{k}: {hex(v)}")
                else:
                    items.append(f"{k}: {v!r}")
            return f"{{{', '.join(items)}}}"

        def run(dut: Decoder):
            with self.subTest(encoded=hex(encoded),
                              illegal=illegal,
                              expected=values_str(expected)):
                yield dut.input.eq(encoded)
                yield Delay(1e-6)
                illegal_v = yield dut.illegal
                result = {}
                for k, expected_v in expected.items():
                    v = yield getattr(dut.output, k)
                    if isinstance(expected_v, Enum):
                        try:
                            v = type(expected_v)(v)
                        except ValueError:
                            pass
                    result[k] = v
                self.assertEqual(illegal, illegal_v)
                self.assertTrue(expected == result, f"expected doesn't match "
                                f"result, result={values_str(result)}")
        return run

    def legal_case(self, opcode: Opcode,
                   operand_type: OperandType, *,
                   rt: Register = Register.Zero, ra: Register = Register.Zero,
                   rb: Register = Register.Zero, rc: Register = Register.Zero,
                   immediate: Union[int, AtomicImmediate] = 0):
        return self.case(encoded=Instruction.encode(opcode,
                                                    operand_type,
                                                    rt=rt,
                                                    ra=ra,
                                                    rb=rb,
                                                    rc=rc,
                                                    immediate=immediate),
                         illegal=False,
                         opcode=opcode,
                         operand_type=operand_type,
                         rt=rt,
                         ra=ra,
                         rb=rb,
                         rc=rc,
                         immediate=immediate)

    def illegal_case(self, encoded: int):
        return self.case(encoded=encoded,
                         illegal=True,
                         opcode=Opcode.Illegal,
                         operand_type=OperandType(0),
                         rt=Register.Zero,
                         ra=Register.Zero,
                         rb=Register.Zero,
                         rc=Register.Zero,
                         immediate=0)

    def cases(self):
        yield self.illegal_case(0)
        yield self.illegal_case(Instruction.encode(Opcode.IllegalLast))
        yield self.illegal_case(Instruction.encode(Opcode.Fence,
                                                   immediate=0x1000))
        yield self.illegal_case(Instruction.encode(Opcode.Div,
                                                   immediate=1))
        yield self.illegal_case(Instruction.encode(Opcode.Div,
                                                   rc=Register(5)))
        yield self.legal_case(Opcode.MulAdd, OperandType.U32, rt=Register(5),
                              ra=Register(10), rb=Register(15), rc=Register(7),
                              immediate=0x12345678)
        yield self.legal_case(Opcode.BranchLE, OperandType.I64, rt=Register(5),
                              ra=Register(10), rb=Register(15), rc=Register(7),
                              immediate=0x12345678)
        yield self.legal_case(Opcode.Fence, OperandType(0),
                              immediate=AtomicImmediate.Acquire)
        yield self.legal_case(Opcode.Fence, OperandType(0),
                              immediate=AtomicImmediate.IFence)

    def test(self):
        dut = Decoder()

        def process():
            for case in self.cases():
                yield from case(dut)
        with get_sim(dut, self,
                     traces=[dut.input, dut.illegal,
                             *dut.output.signals()]) as sim:
            sim.add_process(process)
            sim.run()
