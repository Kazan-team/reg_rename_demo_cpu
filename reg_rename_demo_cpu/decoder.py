# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information

from nmigen.hdl.ast import Signal
from nmigen.hdl.dsl import Module
from nmigen.hdl.ir import Elaboratable
from reg_rename_demo_cpu.instruction import Instruction, Opcode, OperandType, Register
from reg_rename_demo_cpu.util import value_in_range


class Decoder(Elaboratable):
    def __init__(self):
        self.output = Instruction()
        self._decomposed = Instruction()

        def make_sig(operand_type: OperandType):
            return Signal(name=f"_supported_operand_type_{operand_type.name}")
        self._supported_operand_types = {i: make_sig(i) for i in OperandType}
        self._has_rt = Signal()
        self._has_ra = Signal()
        self._has_rb = Signal()
        self._has_rc = Signal()
        self.illegal = Signal()
        self.input = Signal(Instruction.ENCODED_SHAPE)

    def elaborate(self, platform):
        m = Module()
        m.d.comb += self._decomposed.encoded().eq(self.input)
        m.d.comb += self.illegal.eq(False)
        m.d.comb += [i.eq(0) for i in self._supported_operand_types.values()]
        with m.Switch(self._decomposed.opcode):
            for opcode in Opcode:
                if opcode.is_illegal:
                    continue
                with m.Case(opcode):
                    with m.If(~value_in_range(self._decomposed.immediate,
                                              opcode.immediate_range)):
                        m.d.comb += self.illegal.eq(True)
                    for operand_type in opcode.supported_operand_types:
                        s = self._supported_operand_types[operand_type]
                        m.d.comb += s.eq(True)
                    m.d.comb += self._has_rt.eq(opcode.has_rt)
                    m.d.comb += self._has_ra.eq(opcode.has_ra)
                    m.d.comb += self._has_rb.eq(opcode.has_rb)
                    m.d.comb += self._has_rc.eq(opcode.has_rc)
            with m.Default():
                m.d.comb += self.illegal.eq(True)
        with m.If(~self._has_rt & (self._decomposed.rt != Register.Zero)):
            m.d.comb += self.illegal.eq(True)
        with m.If(~self._has_ra & (self._decomposed.ra != Register.Zero)):
            m.d.comb += self.illegal.eq(True)
        with m.If(~self._has_rb & (self._decomposed.rb != Register.Zero)):
            m.d.comb += self.illegal.eq(True)
        with m.If(~self._has_rc & (self._decomposed.rc != Register.Zero)):
            m.d.comb += self.illegal.eq(True)
        with m.Switch(self._decomposed.operand_type):
            for operand_type, s in self._supported_operand_types.items():
                with m.Case(operand_type):
                    with m.If(~s):
                        m.d.comb += self.illegal.eq(True)
            with m.Default():
                m.d.comb += self.illegal.eq(True)
        with m.If(self.illegal):
            m.d.comb += [s.eq(0) for s in self.output.signals()]
            m.d.comb += self.output.opcode.eq(Opcode.Illegal)
        with m.Else():
            m.d.comb += self.output.eq(self._decomposed)
        return m
