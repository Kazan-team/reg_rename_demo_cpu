Simple CPU with register-renaming, out-of-order execution, speculative
execution, branch prediction, and superscalar microarchitecture.