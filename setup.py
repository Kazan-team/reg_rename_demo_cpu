# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information
from setuptools import setup, find_packages

setup(
    name="reg_rename_demo_cpu",
    author="Jacob Lifshay",
    author_email="programmerjake@gmail.com",
    description="Simple CPU with register-renaming, out-of-order execution, "
    "speculative execution, branch prediction, and superscalar "
    "microarchitecture.",
    license="LGPL-2.1-or-later",
    license_files=["LICENSE.md", "Notices.txt"],
    python_requires="~=3.7",
    setup_requires=["setuptools_scm>=6.2"],
    install_requires=[
        "nmigen",
        "nmutil",
    ],
    packages=find_packages(),
)
